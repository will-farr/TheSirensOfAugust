\documentclass[modern]{aastex62}

\usepackage{acro}
\usepackage{amsmath}
\usepackage{amssymb}

\DeclareAcronym{BBH}{
  short=BBH,
  long={binary black hole}
}
\DeclareAcronym{BNS}{
  short=BNS,
  long={binary neutron star}
}
\DeclareAcronym{OOne}{
  short=O1,
  long={LIGO's first observing run}
}
\DeclareAcronym{OTwo}{
  short=O2,
  long={LIGO's second observing run}
}

\newcommand{\sigmaOneSigma}{0.54^{+0.20}_{-0.47}}

\DeclareMathOperator{\Poisson}{Poisson}

\begin{document}

\title{The Sirens of August: Detection of Five Gravitational Wave Events in August 2017 Is Consistent With A Constant Rate}

\author[0000-0003-1540-8562]{Will M. Farr}
\affiliation{Department of Physics and Astronomy, Stony Brook University, Stony Brook NY 11794, USA}
\affiliation{Center for Computational Astronomy, Flatiron Institute, 162 5th Ave., New York NY 10010, USA}
\email{will.farr@stonybrook.edu}

\author{Jonathan R. Gair}
\affiliation{School of Mathematics, University of Edinburgh, Edinburgh EH9 3FD, United Kingdom}
\email{J.Gair@ed.ac.uk}

\section*{}

August 2017 was a happy time for gravitational wave astronomy. Of the 11
gravitational waves---10 \ac{BBH} mergers and one \ac{BNS} merger---detected to
date by the Advanced LIGO \citep{aLIGO} and Advanced Virgo \citep{aVirgo}
instruments, five occurred in August 2017 \citep{O2Catalog}: GW170809, GW170814
\citep{GW170814}, GW170817 \citep{GW170817}, GW170818, and GW170823.  Throughout
\ac{OTwo} there were $\left( 1, 0, 0, 0, 1, 1, 5 \right)$ gravitational waves
from merging compact binary systems detected in January, February, March, April,
June, July, and August 2017 (we omit May because it comprises negligible
observing time; \ac{OTwo} finished in August 2017).  In \ac{OOne} there were
$(1, 1, 1)$ compact binary detections in September, October, and December 2015
(we omit November because it also comprises negligible observing time).  Here we
show that the outlier of five detections in August is nonetheless consistent
with a constant monthly detection rate.  A constant spacetime merger rate
density is the starting point of merger rate analyses in \citet{O2Catalog} and
\citet{O2Populations}; given the $\sim 5 \, \mathrm{Gyr}$ of cosmic time probed
by the \ac{OTwo} observations and the $\sim 10 \, \mathrm{Myr}$ lower limit on
the stellar evolution timescale of even the most massive \ac{BBH} progenitor
systems, fluctuations in the detection rate would be overwhelmingly likely to be
of terrestrial origin and could indicate un-modeled systematic effects in the
instruments.

We ignore effects at the $\sim 10\%$ level that could affect the detection rate,
such as: the varying length of calendar months, variations in detector
sensitivity over \ac{OOne} and \ac{OTwo}, that the sensitivity in \ac{OOne} is
about $3/4$ that of \ac{OTwo}, variations in duty cycle over \ac{OTwo}, the
addition of Advanced Virgo to the detector network in August 2017, etc.  There
were eleven detections in ten months.  The probability that a Poisson process
with a mean rate of $11/10$ per month will have five or more events in one month
is $\simeq 0.54\%$.  Over ten months, the probability that at least one month
will have five or more detections is $\simeq 5.3\%$.  Thus it is not
particularly surprising that we see one month with five detections.

The above simplistic analysis is but one choice from an entire menu of possible
calculations.  For example, we could ask whether the detections in August are
consistent with the observed rate from the previous six months' of data taking
(answer: much less so); or we could ask whether the detections in the second
half of the combined \ac{OOne} and \ac{OTwo} runs are consistent with the rate
from the first half (answer: much more so).  Or....  Most calculations from this
menu will arrive at probabilities in the $1$--$10\%$ range.  The simplistic
analysis above also ignores that we have statistical uncertainty in the mean
rate; $11/10$ detections per month is the most likely value, but the statistical
uncertainty is $\sim 1/\sqrt{11} \simeq 30$\%. This uncertainty will also vary
as the data are grouped differently in each possible calculation discussed
above.

An elegant way to simultaneously address the multiple comparisons problem and
rate measurement uncertainty is through multilevel modeling\footnote{Multilevel
modeling is sometimes also called hierarchical modeling or population analysis.
In an astrophysical context, see \citet{Hogg2010}, \citet{Mandel2010}, or,
applied to gravitational waves, \citet{O2Populations}.} \citep{Gelman2006}.  The
starting point for a multilevel analysis is an independent fit of a Poisson rate
to each months' data:
%
\begin{equation}
  n_i \sim \Poisson\left( \Lambda_i \right),
\end{equation}
%
where $n_i$ is the number of detected events in month $i$ and $\Lambda_i$ is the
Poisson mean rate of detections in that month.  At this point each months' rate
is treated independently of the other months; this is certainly not an optimal
model, since there is certainly \emph{some} month-to-month correlation.  If we
impose that the $\Lambda_i$ are drawn from a common distribution, say a
normal:
%
\begin{equation}
  \label{eq:rate-shrinkage-prior}
  \Lambda_i \sim N(\mu, \sigma)
\end{equation}
%
then the parameter $\mu$ measures the mean of the monthly detection rate and
$\sigma$ measures the amount of month-to-month scatter.  The posterior
distribution for the $\Lambda_i$, $\mu$, and $\sigma$ given the monthly event
counts $n_i$ is therefore
%
\begin{equation}
  \label{eq:full-posterior}
  \pi\left( \Lambda_i, \mu, \sigma \mid n_i \right) \propto \left[ \prod_{i=1}^{N_\mathrm{month}} \frac{\Lambda_i^{n_i}}{n_i!} \exp\left[ -\Lambda_i \right] \phi\left( \Lambda_i \mid \mu, \sigma \right) \right] p\left( \mu, \sigma \right),
\end{equation}
%
where $p\left( \mu, \sigma \right)$ is the prior we impose on $\mu$ and $\sigma$
and $\phi\left(x \mid \mu, \sigma\right)$ is the probability density for a
Gaussian with mean $\mu$ and standard deviation $\sigma$ evaluated at $x$.

In this model, setting $\sigma \equiv 0$ forces a constant monthly rate; setting
$\sigma \to \infty$, we recover independent fits to each months' data. Reality
is somewhere in between; the key insight of multilevel modeling is that we can
allow the \emph{data} to inform the value of $\sigma$ and the degree of
``shrinkage'' \citep[e.g.][]{Lieu2017} or ``pooling'' of the data in the
fit\footnote{Eq.\ \eqref{eq:rate-shrinkage-prior} corresponds to imposing a
white Gaussian process prior on the log detection rate.  A natural extension
would be to introduce a temporal correlation length
\citep{Foreman-Mackey2014,Kelly2014,Foreman-Mackey2017,Farr2018} in the prior so
that the rate variations have a degree of smoothness from month-to-month. But at
this point we are probably getting too far out ahead of our small data set....}.
The multilevel model effectively explores all possible pooled combinations of
the data as $\sigma$ varies, and chooses those that are consistent with the
observed month-to-month scatter, while accounting for the multiple comparisons
and the varying accuracy of the overall rate estimation with various poolings.

The result of a fit for the eleven $\Lambda_i$ and $\mu$ and $\sigma$ to the
number of detections by month is shown in Figure \ref{fig:rates-and-sigma}.  We
used broad $N(0,10)$ priors for both $\mu$ and $\sigma$.  Such broad priors have
negligible effect on the posterior given the precision of our measurements of
$\mu$ and $\sigma$.  The partially-pooled posteriors for the monthly rates
$\Lambda_i$ overlap significantly, suggesting that a constant monthly detection
rate is consistent with our data set.  This is confirmed by the posterior for
the month-to-month scatter in the rate, which includes $\sigma = 0$ in its 95\%
(``2$\sigma$'') credible interval.  We therefore conclude that five detections
in August are consistent with a constant monthly detection rate throughout
\ac{OTwo}.

\begin{figure}
  \plottwo{rates-posterior}{sigma}
%
  \caption{\label{fig:rates-and-sigma} Inference on the per-month detection rate
  and the scatter parameter describing the population of per-month rates.  Left:
  the posterior on the mean per-month detection rate, $\Lambda$, for each
  calendar month of \ac{OOne} and \ac{OTwo} with significant observing time. The
  curves are labeled by the month index (1 = January, 2 = February, etc). The
  August outlier is apparent in the posterior for $\Lambda_8$.  Dashed curves
  give the posterior for a flat prior imposed on each of the $\Lambda_i$; solid
  curves give the posterior under the multilevel log-normal prior.  It is
  apparent that this latter ``shrinks'' the monthly estimates toward a common
  rate.  For both the independent and pooled analyses there is some overlap in
  the posteriors, suggesting that a constant monthly detection rate is
  consistent with our inferences. Right: the posterior on the parameter $\sigma$
  that controls the scatter in the population of (log) detection rates.  We
  obtain $\sigma = \sigmaOneSigma$ (median and 68\% CI); the solid line
  indicates the median value, dashed lines the 68\% CI, and dotted lines the
  95\% CI (which includes $\sigma = 0$).}
%
\end{figure}

\acknowledgments

This document is LIGO Technical Report Number
\href{https://dcc.ligo.org/T1800529/public}{T1800529}.  A \texttt{git}
repository containing analysis code and this \LaTeX{} document can be found at
\url{https://git.ligo.org/will-farr/TheSirensOfAugust}.

\software{matplotlib \citep{Hunter2007}, seaborn
\url{https://doi.org/10.5281/zenodo.1313201}, pymc3 \citep{Salvatier2016}}

\newpage

\bibliography{sirens}

\end{document}
