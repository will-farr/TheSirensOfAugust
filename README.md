With apologies to [Barbara Tuchman](https://en.wikipedia.org/wiki/The_Guns_of_August),
a brief note on why the unprecedented number of gravitational wave signals
detected in Aug 2017 is nevertheless statistically within expected fluctuations
in detection rates.

This repository auto-builds the latest version of the associated note to
[PDF](https://git.ligo.org/will-farr/TheSirensOfAugust/-/jobs/artifacts/master/file/note/sirens.pdf?job=pdf)
after each push.
